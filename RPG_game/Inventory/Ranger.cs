﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Creates Ranger object with given base and level-up attribute stats, compatibility check, 
    /// and primary attribute (attribute that affects damage)
    /// </summary>
    public class Ranger : Hero
    {
        public override string Class { get; } = "Ranger";
        protected override string[] ClassWeapons { get; } = { "Bow" };
        protected override string[] ClassArmor { get; } = { "Leather", "Mail" };
        public override PrimaryAttributes Stats { get; set; } = new PrimaryAttributes() { Stats = new int[] { 1, 7, 1 } };
        public override PrimaryAttributes LevelStats { get; } = new PrimaryAttributes() { Stats = new int[] { 1, 5, 1 } };
        public override int Prim { get; } = 1;

        /// <summary>
        /// Calls parent constructor
        /// </summary>
        public Ranger(string newName) : base(newName) { }
    }
}
