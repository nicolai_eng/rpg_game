﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Creates dps stats for weapon
    /// </summary>
    public class WeaponAttributes
    {
        public int Damage { get; set; } = 1;
        public double AttackSpeed { get; set; } = 1;
        public double Dps { get; set; } = 1;

        /// <summary>
        /// Default constructor
        /// </summary>
        public WeaponAttributes() { }

        /// <summary>
        /// Constructs damage stats depending on level.
        /// </summary>
        public WeaponAttributes(int level)
        {
            Damage = (int)Math.Ceiling(level / 2.0) + Randomizer.Rand(4);
            AttackSpeed = 0.5 + Randomizer.Rand(7) / 4.0;
            Dps = Damage * AttackSpeed;
        }
        public WeaponAttributes(int damage, double speed)
        {
            Damage = damage;
            AttackSpeed = speed;
            Dps = Damage * AttackSpeed;
        }
    }
}
