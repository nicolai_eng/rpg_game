﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Creates Rogue object with given base and level-up attribute stats, compatibility check, 
    /// and primary attribute (attribute that affects damage)
    /// </summary>
    public class Rogue : Hero
    {
        public override string Class { get; } = "Rogue";
        protected override string[] ClassWeapons { get; } = { "Dagger", "Sword" };
        protected override string[] ClassArmor { get; } = { "Leather", "Mail" };
        public override PrimaryAttributes Stats { get; set; } = new PrimaryAttributes() { Stats = new int[] { 2, 6, 1 } };
        public override PrimaryAttributes LevelStats { get; } = new PrimaryAttributes() { Stats = new int[] { 1, 4, 1 } };
        public override int Prim { get; } = 1;

        /// <summary>
        /// Calls parent constructor
        /// </summary>
        public Rogue(string newName) : base(newName) { }
    }
}
