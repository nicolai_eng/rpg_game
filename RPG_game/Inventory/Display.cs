﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace RPG_game.Inventory
{
    /// <summary>
    /// Displays stats of items and heroes.
    /// </summary>
    internal static class Display
    {
        /// <summary>
        /// Prints out weapon stats.
        /// </summary>
        public static void WeaponStats(Weapons item)
        {
            StringBuilder stats = new StringBuilder();
            stats.AppendLine("************");
            stats.AppendLine($"{item.Type} ({item.Slot})");
            stats.AppendLine($"Level: {item.ItemLevel}");
            stats.AppendLine("Stats:");
            stats.AppendLine($"  Damage: {item.Attributes.Damage}");
            stats.AppendLine($"  Attack speed: {item.Attributes.AttackSpeed}");
            stats.AppendLine($"  DPS: {item.Attributes.Dps}");
            stats.AppendLine("************");
            Console.WriteLine(stats.ToString());
        }

        /// <summary>
        /// Prints out armor stats.
        /// </summary>
        public static void ArmorStats(Armor item)
        {
            StringBuilder stats = new StringBuilder();
            stats.AppendLine("************");
            stats.AppendLine($"{item.Type} ({item.Slot})");
            stats.AppendLine($"Level: {item.ItemLevel}");
            stats.AppendLine("Stats:");
            stats.AppendLine($"  Strength: {item.Attributes.Stats[0]}");
            stats.AppendLine($"  Dexterity: {item.Attributes.Stats[1]}");
            stats.AppendLine($"  Intelligence: {item.Attributes.Stats[2]}");
            stats.AppendLine("************");
            Console.WriteLine(stats.ToString());
        }

        /// <summary>
        /// Prints out hero stats.
        /// </summary>
        public static void HeroStats(Hero hero)
        {
            StringBuilder stats = new StringBuilder();
            stats.AppendLine("\n************");
            stats.AppendLine($"{hero.Name}");
            stats.AppendLine($"Class: {hero.Class} (Level {hero.Level})");
            stats.AppendLine("Stats:");
            stats.AppendLine($"  Strength: {hero.Stats[0]}");
            stats.AppendLine($"  Dexterity: {hero.Stats[1]}");
            stats.AppendLine($"  Intelligence: {hero.Stats[2]}");
            stats.AppendLine($"  Damage: {hero.Damage}");
            stats.AppendLine("************");
            Console.WriteLine(stats.ToString());
        }

    }
}
