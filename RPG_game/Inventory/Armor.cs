﻿using System;

namespace RPG_game.Inventory
{
    /// <summary>
    /// Creates armor objects that hero can equip, altering their attribute stats.
    /// </summary>
    public class Armor : Items
    {
        private string[] ArmorTypeList { get; } = { "Head", "Body", "Legs" };
        public string[] MaterialList { get; } = { "Cloth", "Leather", "Mail", "Plate" };
        public PrimaryAttributes Attributes { get; set; }

        /// <summary>
        /// Constructor creating random armor slot, type and item attributes. 
        /// </summary>
        public Armor()
        {
            Slot = ArmorTypeList[Randomizer.Rand(ArmorTypeList.Length)];
            Type = MaterialList[Randomizer.Rand(MaterialList.Length)];
            Attributes = new PrimaryAttributes(ItemLevel);
        }
    }
}
