﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Creates Mage object with given base and level-up attribute stats, compatibility check, 
    /// and primary attribute (attribute that affects damage)
    /// </summary>
    public class Mage : Hero
    {
        public override string Class { get; } = "Mage";
        protected override string[] ClassWeapons { get; } = { "Staff", "Wand" };
        protected override string[] ClassArmor { get; } = { "Cloth" };
        public override PrimaryAttributes Stats { get; set; } = new PrimaryAttributes() { Stats = new int[] { 1, 1, 8 } };
        public override PrimaryAttributes LevelStats { get; } = new PrimaryAttributes() { Stats = new int[] { 1, 1, 5 } };
        public override int Prim { get; } = 2;

        /// <summary>
        /// Calls parent constructor
        /// </summary>
        public Mage(string newName) : base(newName) { }
    }
}
