﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RPG_game.Inventory
{
    /// <summary>
    /// Abstract hero class which is used to create functionality for four hero classes (Mage, Rogue, Ranger and Warrior)  
    /// </summary>
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public abstract int Prim { get; }

        public abstract string Class { get; }
        protected abstract string[] ClassWeapons { get; }
        protected abstract string[] ClassArmor { get; }

        public Dictionary<string, Armor> Armor1 { get; set; }
        public abstract PrimaryAttributes LevelStats { get; }
        public abstract PrimaryAttributes Stats { get; set; }

        public Weapons Weapon { get; set; }
        public double Dps { get; set; } = 1;
        public double Damage { get; set; }

        /// <summary>
        /// Constructor. Sets hero name, updates damage according to base attributes
        /// and istantiates armor dictionary.
        /// </summary>
        public Hero(string newName)
        {
            Name = newName;
            Armor1 = new Dictionary<string, Armor>();
            UpdateDamage();
        }

        /// <summary>
        /// Levels up the character and updates damage.
        /// </summary>
        public void LevelUp()
        {
            Level += 1;
            UpdateStats(LevelStats);
        }

        /// <summary>
        /// Updates stats and damage
        /// </summary>
        public void UpdateStats(PrimaryAttributes addPrim)
        {
            Stats.PrimAdd(addPrim);
            UpdateDamage();
        }
        public void UpdateStats(PrimaryAttributes remPrim, PrimaryAttributes addPrim)
        {
            Stats.PrimRem(remPrim);
            Stats.PrimAdd(addPrim);
            UpdateDamage();
        }

        /// <summary>
        /// Updates damage with current dps and attribute values.
        /// </summary>
        public void UpdateDamage()
        {
            Damage = Dps * (1.0 + Stats[Prim] / 100.0);
        }

        /// <summary>
        /// Equips armor-object in relevant dictionary slot. 
        /// Throws InvalidArmorException if either level type requirements aren't fulfilled.
        /// </summary>
        public string EquipArmor(Armor item)
        {
            if (!ClassArmor.Contains(item.Type))
            {
                throw new InvalidArmorException("Armor not compatible with class");
            }
            else if (Level < item.ItemLevel)
            {
                throw new InvalidArmorException("Item level to high");
            }
            else
            {
                if (Armor1.ContainsKey(item.Slot))
                {
                    UpdateStats(Armor1[item.Slot].Attributes, item.Attributes);
                    Armor1[item.Slot] = item;
                }
                else
                {
                    UpdateStats(item.Attributes);
                    Armor1.Add(item.Slot, item);
                }
            }
            return "New armor equiped";
        }

        /// <summary>
        /// Equips weapon-object.
        /// Throws InvalidWeaponException if either level type requirements aren't fulfilled.
        /// </summary>
        public string EquipWeapon(Weapons item)
        {
            if (!ClassWeapons.Contains(item.Type))
            {
                throw new InvalidWeaponException("Weapon not compatible with class");
            }
            else if (Level < item.ItemLevel)
            {
                throw new InvalidWeaponException("Item level to high");
            }
            else
            {
                Weapon = item;
                Dps = item.Attributes.Dps;
                UpdateDamage();
            }
            return "New weapon equiped";
        }
    }
}
