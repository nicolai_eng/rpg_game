﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Exception thrown when equipping weapon while requirements (level or compatibility) isn't met.
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string name)
            : base($"\nEquipment error: {name}")

        {

        }
    }
}
