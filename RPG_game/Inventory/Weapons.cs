﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Creates weapon objects that hero can equip, altering their dps stats-
    /// </summary>
    public class Weapons : Items
    {
        private string[] WeapTypeList { get; } = { "Axe", "Bow", "Hammer", "Staff", "Sword", "Wand" };
        public WeaponAttributes Attributes { get; set; }

        /// <summary>
        /// Constructor creating random weapon type and item attributes. 
        /// </summary>
        public Weapons()
        {
            Slot = "Weapon";
            Type = WeapTypeList[Randomizer.Rand(WeapTypeList.Length)];
            Attributes = new WeaponAttributes(ItemLevel);
        }
    }
}
