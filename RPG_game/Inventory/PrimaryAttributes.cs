﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Creates attribute stats for armor and hero, together with stat-object addition features.
    /// </summary>
    public class PrimaryAttributes
    {
        public int[] Stats { get; set; } = new int[3] { 0, 0, 0 };
        public int this[int i] { get => Stats[i]; set => Stats[i] = value; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public PrimaryAttributes() { }

        /// <summary>
        /// Sets attribute values depending on item level.
        /// </summary>
        public PrimaryAttributes(int level)
        {
            for (int i = 0; i < Stats.Length; i++)
            {
                Stats[i] = (int)Math.Ceiling(level / 2.0) + Randomizer.Rand(4);
            }
        }

        /// <summary>
        /// Adds input object stats to stats
        /// </summary>
        public void PrimAdd(PrimaryAttributes addPrim)
        {
            for (int i = 0; i < 3; i++)
            {
                Stats[i] += addPrim[i];
            }
        }

        /// <summary>
        /// Subtracts input object stats to stats
        /// </summary>
        public void PrimRem(PrimaryAttributes remPrim)
        {
            for (int i = 0; i < 3; i++)
            {
                Stats[i] -= remPrim[i];
            }
        }
    }
}
