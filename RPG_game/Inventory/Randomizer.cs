﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_game.Inventory
{
    /// <summary>
    /// Generates random integer number from input.
    /// </summary>
    public static class Randomizer
    {
        public static Random rnd = new Random();

        /// <summary>
        /// Generates random integer number between 0 and (length - 1). 
        /// </summary>
        public static int Rand(int length)
        {
            return rnd.Next(length);
        }

    }
}
