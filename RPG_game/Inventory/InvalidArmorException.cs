﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Exception thrown when equipping armor while requirements (level or compatibility) isn't met.
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string name)
            : base($"\nEquipment error: {name}")
        {
        }
    }
}
