﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Abstract item class which is used to create functionality for two item classes (Weapons and Armor).
    /// </summary>
    public abstract class Items
    {
        public int ItemLevel { get; set; }
        public string? Slot { get; set; }
        public string? Type { get; set; }

        /// <summary>
        /// Creates a random level for the item.
        /// </summary>
        public Items()
        {
            ItemLevel = Randomizer.Rand(8);
        }
    }
}
