﻿namespace RPG_game.Inventory
{
    /// <summary>
    /// Creates Warrior object with given base and level-up attribute stats, compatibility check, 
    /// and primary attribute (attribute that affects damage)
    /// </summary>
    public class Warrior : Hero
    {
        public override string Class { get; } = "Warrior";
        protected override string[] ClassWeapons { get; } = { "Axe", "Hammer", "Sword" };
        protected override string[] ClassArmor { get; } = { "Mail", "Plate" };
        public override PrimaryAttributes Stats { get; set; } = new PrimaryAttributes() { Stats = new int[] { 5, 2, 1 } };
        public override PrimaryAttributes LevelStats { get; } = new PrimaryAttributes() { Stats = new int[] { 3, 2, 1 } };
        public override int Prim { get; } = 0;

        /// <summary>
        /// Calls parent constructor
        /// </summary>
        public Warrior(string newName) : base(newName) { }
    }
}
