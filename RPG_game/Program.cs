﻿using RPG_game.Inventory;

namespace RPG_game
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Game title:
            Console.WriteLine("Welcome to: Goblin beating simulator 2.0!\n");

            StartPage();

            /// Choice between creating character and closing application. Loops for invalid inputs.
            void StartPage()
            {
                Console.WriteLine("(c): Create new character.");
                Console.WriteLine("(e): Exit application.");

                switch (Console.ReadLine())
                {
                    case "c":
                        CreateCharacter();
                        break;
                    case "e":
                        return;
                    default:
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }

            /// Creates a hero object. User selects name and hero character class (Mage, Rogue, Ranger or Warrior).
            void CreateCharacter()
            {
                Console.Write("\nChoose name: ");
                string Name = Console.ReadLine() ?? "Sven";

                Console.WriteLine("\nChoose character type:");
                Console.WriteLine("(1): Mage");
                Console.WriteLine("(2): rogue");
                Console.WriteLine("(3): Ranger");
                Console.WriteLine("(4): Warrior");

                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            Mage mage = new Mage(Name);
                            MainOptions(mage);
                            return;
                        case "2":
                            Rogue rogue = new Rogue(Name);
                            MainOptions(rogue);
                            return;
                        case "3":
                            Ranger ranger = new Ranger(Name);
                            MainOptions(ranger);
                            return;
                        case "4":
                            Warrior warrior = new Warrior(Name);
                            MainOptions(warrior);
                            return;
                        default:
                            Console.WriteLine("Invalid input!\n");
                            break;
                    }
                }
            }

            /// Shows main options that a hero can take:
            /// 1. Search for item
            /// 2. Find monster
            /// 3. Show hero stats
            /// 4. Exit application.
            void MainOptions(Hero hero)
            {
                while (true)
                {
                    Console.WriteLine("\nMain menu:");
                    Console.WriteLine("(i): Search for item.");
                    Console.WriteLine("(f): Find monster");
                    Console.WriteLine("(s): Show character stats.");
                    Console.WriteLine("(e): Exit application.");

                    switch (Console.ReadLine())
                    {
                        case "i":
                            SearchForItem(hero);
                            break;
                        case "f":
                            FightMonster(hero);
                            break;
                        case "s":
                            ShowStats(hero);
                            break;
                        case "e":
                            return;
                        default:
                            Console.WriteLine("Invalid input!");
                            break;
                    }
                }
            }

            /// 1. Search for item: hero finds an auto generated item. Then the Equip item function is called.
            void SearchForItem(Hero hero)
            {
                Random rnd = new Random();
                Console.WriteLine("\nYou found:");

                if (rnd.Next(3) == 0)
                {
                    Weapons weapon = new Weapons();
                    Display.WeaponStats(weapon);
                    EquipItem(hero, weapon, "Weapon");
                }
                else
                {
                    Armor armor = new Armor();
                    Display.ArmorStats(armor);
                    EquipItem(hero, armor, "Armor");
                }
            }

            /// Hero can equip or discard item-type. If equipping and item is not compatible with hero (material or level)
            /// an exception will be thrown (InvalidArmorException or InvalidWeaponException).
            void EquipItem<T>(Hero hero, T item, string type)
            {
                while (true)
                {
                    Console.WriteLine("Do you wanna equip item? (y/n)");

                    switch (Console.ReadLine())
                    {
                        case "y":
                            try
                            {
                                string m;
                                if (type == "Weapon") { m = hero.EquipWeapon((Weapons)(object)item); }
                                else { m = hero.EquipArmor((Armor)(object)item); }
                                Console.WriteLine(m);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            return;
                        case "n":
                            Console.WriteLine($"\n{type} not equiped");
                            return;
                        default:
                            Console.WriteLine("\nInvalid input!");
                            break;
                    }
                }
            }

            /// Generates a monster. Hero can either fight monster (which will trigger a level up), or flee.
            void FightMonster(Hero hero)
            {
                Console.WriteLine("\nYou face a goblin.");
                Console.WriteLine("Fight? (y/n)");

                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "y":
                            Console.WriteLine("\nYou won!");
                            Console.WriteLine($"Level up: {hero.Level} -> {hero.Level + 1}");
                            hero.LevelUp();
                            return;
                        case "n":
                            Console.WriteLine("\nYou ran... coward!");
                            return;
                        default:
                            Console.WriteLine("\nInvalid input!");
                            break;
                    }
                }
            }

            /// Shows hero stats
            void ShowStats(Hero hero)
            {
                Display.HeroStats(hero);
            }

        }
    }
}
