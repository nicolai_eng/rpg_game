﻿
using RPG_game.Inventory;

namespace RPG_gameTest
{
    public class ItemsAndEquipmentTests
    {
        [Fact]
        public void EquipWeapon_ToHighLevel_ShouldThrowWeaponException()
        {
            // Arrange
            Weapons testAxe = new Weapons()
            {
                ItemLevel = 2,
                Slot = "Weapon",
                Type = "Axe",
                Attributes = new WeaponAttributes()
            };

            Warrior warrior = new Warrior("Sven");

            // Assert
            Assert.Throws<InvalidWeaponException>((() =>warrior.EquipWeapon(testAxe)));
        }

        [Fact]
        public void EquipArmor_ToHighLevel_ShouldThrowArmorException()
        {
            // Arrange
            Armor testPlateBody = new Armor()
            {
                ItemLevel = 2,
                Slot = "Body",
                Type = "Plate",
                Attributes = new PrimaryAttributes()
            };

            Warrior warrior = new Warrior("Sven");

            // Assert
            Assert.Throws<InvalidArmorException>((() => warrior.EquipArmor(testPlateBody)));
        }

        [Fact]
        public void EquipWeapon_IncompitableType_ShouldThrowWeaponException()
        {
            // Arrange
            Weapons testBow = new Weapons()
            {
                ItemLevel = 1,
                Slot = "Weapon",
                Type = "Bow",
                Attributes = new WeaponAttributes()
            };

            Warrior warrior = new Warrior("Sven");

            // Assert
            Assert.Throws<InvalidWeaponException>((() => warrior.EquipWeapon(testBow)));
        }

        [Fact]
        public void EquipArmor_IncompitableType_ShouldThrowArmorException()
        {
            // Arrange
            Armor TestClothHead = new Armor()
            {
                ItemLevel = 2,
                Slot = "Head",
                Type = "Cloth",
                Attributes = new PrimaryAttributes()
            };

            Warrior warrior = new Warrior("Sven");

            // Assert
            Assert.Throws<InvalidArmorException>((() => warrior.EquipArmor(TestClothHead)));
        }

        [Fact]
        public void EquipWeapon_Success_ShouldReturnEquipMessage()
        {
            // Arrange
            Weapons testAxe = new Weapons()
            {
                ItemLevel = 1,
                Slot = "Weapon",
                Type = "Axe",
                Attributes = new WeaponAttributes()
            };

            Warrior warrior = new Warrior("Sven");

            string expect = "New weapon equiped";

            // Act
            string actual = warrior.EquipWeapon(testAxe);
            Assert.Equal(expect, actual);
        }

        [Fact]
        public void EquiArmor_Success_ShouldReturnEquipMessage()
        {
            // Arrange
            Armor testPlateBody = new Armor()
            {
                ItemLevel = 1,
                Slot = "Body",
                Type = "Plate",
                Attributes = new PrimaryAttributes()
            };

            Warrior warrior = new Warrior("Sven");

            string expect = "New armor equiped";

            // Act
            string actual = warrior.EquipArmor(testPlateBody);

            // Assert
            Assert.Equal(expect, actual);
        }

        [Fact]
        public void NoWeapon_DamageOutput_ShouldThrowProductofDPSAndAttributeModifier()
        {
            // Arrange
            Warrior warrior = new Warrior("Sven");

            var expected = 1 + 5 / 100.0;

            // Act
            var actual = warrior.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeapon_DamageOutput_ShouldBeProductOfDPSAndAttributeModifier()
        {
            // Arrange
            Weapons testAxe = new Weapons()
            {
                ItemLevel = 1,
                Slot = "Weapon",
                Type = "Axe",
                Attributes = new WeaponAttributes(7, 1.1)
            };

            Warrior warrior = new Warrior("Sven");
            warrior.EquipWeapon(testAxe);

            var expected = (7 * 1.1) * (1 + 5 / 100.0);

            // Act
            var actual = warrior.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeaponAndArmor_DamageOutput_ShouldBeProductOfDPSAndAttributeModifier()
        {
            // Arrange
            Weapons testAxe = new Weapons()
            {
                ItemLevel = 1,
                Slot = "Weapon",
                Type = "Axe",
                Attributes = new WeaponAttributes(7, 1.1)
            };

            Armor testPlateBody = new Armor()
            {
                ItemLevel = 1,
                Slot = "Body",
                Type = "Plate",
                Attributes = new PrimaryAttributes() { Stats = new int[] {1, 0, 0}}
            };

            Warrior warrior = new Warrior("Sven");
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody);

            var expected = (7 * 1.1) * (1 + (5 + 1) / 100.0);

            // Act
            var actual = warrior.Damage;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
