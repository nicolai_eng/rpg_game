using RPG_game.Inventory;

namespace RPG_gameTest
{
    public class HeroLevelAndAttributeTest
    {
        [Fact]
        public void Warrior_Instantiation_ShouldBeLevelOne()
        {
            // Arrange
            Warrior warrior = new Warrior("Sven");
            
            var expected = 1;

            // Act
            var actual = warrior.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_LevelUp_ShouldBeLevelTwo()
        {
            // Arrange
            Warrior warrior = new Warrior("Sven");
            warrior.LevelUp();

            var expected = 2;

            // Act
            var actual = warrior.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_Instantiating_ShouldHaveCorrectBaseAttributeValues()
        {
            // Arrange
            Warrior warrior = new Warrior("Sven");

            var expected = new int[] {5,2,1};

            // Act
            var actual = warrior.Stats.Stats;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_Instantiating_ShouldHaveCorrectBaseAttributeValues()
        {
            // Arrange
            Mage mage = new Mage("Sven");

            var expected = new int[] { 1, 1, 8 };

            // Act
            var actual = mage.Stats.Stats;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_Instantiating_ShouldHaveCorrectBaseAttributeValues()
        {
            // Arrange
            Rogue rogue = new Rogue("Sven");

            var expected = new int[] { 2, 6, 1 };

            // Act
            var actual = rogue.Stats.Stats;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_Instantiating_ShouldHaveCorrectBaseAttributeValues()
        {
            // Arrange
            Ranger ranger = new Ranger("Sven");

            var expected = new int[] { 1, 7, 1 };

            // Act
            var actual = ranger.Stats.Stats;

            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void Warrior_LevelUp_ShouldHaveCorrectAttributeValues()
        {
            // Arrange
            Warrior warrior = new Warrior("Sven");
            warrior.LevelUp();

            var expected = new int[] { 5+3, 2+2, 1+1 };

            // Act
            var actual = warrior.Stats.Stats;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_LevelUp_ShouldHaveCorrectBaseAttributeValues()
        {
            // Arrange
            Mage mage = new Mage("Sven");
            mage.LevelUp();

            var expected = new int[] { 1+1, 1+1, 8+5 };

            // Act
            var actual = mage.Stats.Stats;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_LevelUp_ShouldHaveCorrectBaseAttributeValues()
        {
            // Arrange
            Rogue rogue = new Rogue("Sven");
            rogue.LevelUp();

            var expected = new int[] { 2+1, 6+4, 1+1 };

            // Act
            var actual = rogue.Stats.Stats;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_LevelUp_ShouldHaveCorrectBaseAttributeValues()
        {
            // Arrange
            Ranger ranger = new Ranger("Sven");
            ranger.LevelUp();

            var expected = new int[] { 1+1, 7+5, 1+1 };

            // Act
            var actual = ranger.Stats.Stats;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}